package resume;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

@Component
public class MailComponent {

	@Autowired
	private MailSender mailSender;

	public void generateMail(String name, String email, String subject, String text) {

		// Create a thread safe "copy" of the template message and customize it
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(email);
		msg.setBcc("lgwen994@gmail.com");
		msg.setSubject("Thank you for your email. Re: " + subject);
		msg.setText(
				"Hi " + name + ",\nThank you for your email. I will read it soon. Have a great day!\n\r"
						+ "Kind regards,\nShane Wen\n***************************\n" + text);
		try {
			this.mailSender.send(msg);
		} catch (MailException ex) {
			// simply log it and go on...
			System.err.println(ex.getMessage());
		}
	}
}
