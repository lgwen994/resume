package resume;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class MainController {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainController.class);
	@Autowired
	private MessageRepository repository;
	@Autowired
	MailComponent mailComponent;
	
	@RequestMapping("/message")
	public String writeMessage(String name, String email, String subject, @RequestParam(name = "message") String text) {
		
		LOG.info("xxmailxxx");
		mailComponent.generateMail(name, email, subject, text);

		repository.save(new Message(name, email, subject, text));
		return "redirect:/index.html#contact";
	}
}
