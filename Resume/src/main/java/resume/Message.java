package resume;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MESSAGE", schema = "public")
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_sequence")  
	@SequenceGenerator(name = "id_sequence", allocationSize = 1, sequenceName = "MESSAGE_SEQUENCE")
	private Long id;

	private String name;
	private String mail;
	private String subject;
	private String text;

	protected Message() {

	}

	public Message(String name, String mail, String subject, String text) {
		this.name = name;
		this.mail = mail;
		this.subject = subject;
		this.text = text;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
